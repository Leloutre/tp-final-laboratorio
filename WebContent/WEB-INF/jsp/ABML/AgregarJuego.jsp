<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>IGDb - Agregar Juego</title>

<!-- Favicon -->
<link rel="icon" href="images/core-img/favicon.ico">

<!-- Stylesheet -->
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
</head>

<body>
	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area wow fadeInDown" data-wow-delay="500ms">
	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div
					class="col-12 d-flex align-items-center justify-content-between">
					<!-- Logo Area -->
					<div class="logo">
						<a href="Inicio.html"><img src="images/core-img/logo.png"
							alt=""></a>
					</div>

					<!-- Search & Login Area -->
					<div class="search-login-area d-flex align-items-center">
						<!-- Top Search Area -->
						<div class="top-search-area">
							<form action="#" method="post">
								<input type="search" name="top-search" id="topSearch"
									placeholder="Buscar">
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</form>
						</div>
						<!-- Login Area -->
						<%
							if (session.getAttribute("User") != null) {
						%>
						<div class="login-area">
		                                <a href="MisDatos.html"><span><%= ((Usuario)session.getAttribute("User")).getUsuarioNombre() %></span></a>
		                            </div>
		                            
		                            <div class="login-area">
		                                <a href="NuevaPeticion.html"><span>Pedir juego faltante</span></a>
		                            </div>
		                            
		                            <form method="post" action="ServletUsuarios">
		                            	<div class="login-area">
			                               <input type="submit" name="btnCerrarSesion" value="Desconectar" style="border-style: none"/>
			                            </div>
		                            </form>

						<%
							} else {
						%>
						<div class="login-area">
							<a href="Login.html"><span>Acceder / Registro</span> <i
								class="fa fa-lock" aria-hidden="true"></i></a>
						</div>
						<%
							}
						%>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="egames-main-menu" id="sticker">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="egamesNav">

				<!-- Navbar Toggler -->
				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<!-- Menu -->
				<div class="classy-menu">

					<!-- Close Button -->
					<div class="classycloseIcon">
						<div class="cross-wrap">
							<span class="top"></span><span class="bottom"></span>
						</div>
					</div>

					<!-- Nav Start -->
					<div class="classynav">
						<ul>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="Buscar.html">Búsqueda avanzada</a></li>
							<%
								if(session.getAttribute("User")!=null)
	                            {
	                            	if( ((Usuario)session.getAttribute("User")).getUsuarioNombre() != null && ((Usuario)session.getAttribute("User")).getUsuarioAdmin())
	                            	{
	                            	%>
	                            		<li><a href="ListarUsuariosAdmin.html">Listar usuarios</a></li>
										<li><a href="ListadoPeticiones.html">Listar peticiones</a></li>
										<li><a href="ABMLDesarrolladores.html">Desarrolladores</a></li>
										<li><a href="ABMLDistribuidores.html">Distribuidores</a></li>
										<li><a href="ABMLGeneros.html">Géneros</a></li>
										<li><a href="ABMLPaises.html">Países</a></li>
										<li><a href="ABMLPlataformas.html">Plataformas</a></li>
										<li><a href="AgregarJuego.html">Agregar Juego</a></li>
	                            	<%
	                            			}
	                            		}
                                    %>
						</ul>
					</div>
					<!-- Nav End -->
				</div>
				</nav>
			</div>
		</div>
	</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<%
		List<Desarrollador> listaDesarrolladores = null;
		List<Distribuidor> listaDistribuidores = null;
		Juego juegoAModif = null;
		//Obtiene los productos del controlador (Servlet)
		if(request.getAttribute("listaDesarrolladores")!=null){
			listaDesarrolladores =  (List<Desarrollador>) (request.getAttribute("listaDesarrolladores"));
		}
		if(request.getAttribute("listaDistribuidores")!=null){
			listaDistribuidores =  (List<Distribuidor>) (request.getAttribute("listaDistribuidores"));
		}
		if(request.getAttribute("juegoAModif")!=null){
			juegoAModif = (Juego)request.getAttribute("juegoAModif");
		}
	%>
	<!-- ##### Breadcrumb Area Start ##### -->
	<section class="breadcrumb-area bg-img bg-overlay"
		style="background-image: url(images/background.jpg);">
	<div class="container h-100">
		<div class="row h-100 align-items-center">
			<!-- Breadcrumb Text -->
			<div class="col-12">
				<div class="breadcrumb-text">
					<h2><%= juegoAModif != null ? "Modificar Juego" : "Agregar Juego"%>  - Paso 1 de 3</h2>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!-- ##### Breadcrumb Area End ##### -->

	<!-- ##### Contact Area Start ##### -->
	<section class="contact-area section-padding-100">
	<div class="container">
		<div class="row">

			<!-- Contact Form Area -->
			<div class="col-12">
				<div class="contact-form-area">
					<form class="row" method="post" action="AgregarJuego2.html" enctype="multipart/form-data">
						<div class="col-12 col-lg-6">
						<input type="hidden" name="idGame" value="<%= juegoAModif != null ? juegoAModif.getJuegoId() : ""%>">
							<h4>Nombre</h4>
							<input  required type="text" class="form-control" id="name" name="nameGame"
								placeholder="Mario" value="<%= juegoAModif != null ? juegoAModif.getJuegoNombre() : ""%>">
							<h4>Sinopsis</h4>
							<textarea  required name="descripGame" class="form-control" id="descripcion"
								cols="30" rows="10" placeholder="¡Rescata a la princesa!"
								style="resize: none"><%= juegoAModif != null ? juegoAModif.getJuegoDescripcion() : ""%></textarea>
							<h4>Desarrollador</h4>
							<select required class="form-control" name="desGame">
								<option value="">None</option>
								  <%
									if(listaDesarrolladores.size() > 0){
										%>
											<c:forEach items="${listaDesarrolladores}" var="abr">
												<option value="${abr.getDesarrolladorId()}">${abr.getDesarrolladorNombre()}</option>
											</c:forEach>
										<%
									}
								%>
							</select>
							
						</div>
						<div class="col-12 col-lg-6">
								<h4>Tapa</h4>
								<input type="file" name="UploadFile" accept=".jpg" class="form-control"> 
								<h6 style="width:50%">Fecha de Lanzamiento:</h6>
								<input required type="date" name="releaseDate" value="<%= juegoAModif != null ? juegoAModif.getJuegoFechaLanzamiento() : ""%>" class="form-control" style="width: 50%; display: inline"> <br><br>
								<% if(juegoAModif != null && juegoAModif.getRegionUS()) {%>
									<input type="checkbox" checked name="releaseCheckUS" value="Lanzado en US"> <h6 style="display: inline">Lanzado en US</h6> <br><br>
								<%} else { %>
									<input type="checkbox" name="releaseCheckUS" value="Lanzado en US"> <h6 style="display: inline">Lanzado en US</h6> <br><br>
								<%} %>
								<% if(juegoAModif != null && juegoAModif.getRegionEU()) {%>
									<input type="checkbox" checked name="releaseCheckEU" value="Lanzado en EU"> <h6 style="display: inline">Lanzado en EU</h6> <br><br>
								<%} else { %>
									<input type="checkbox" name="releaseCheckEU" value="Lanzado en EU"> <h6 style="display: inline">Lanzado en EU</h6> <br><br>
								<%} %>
								<% if(juegoAModif != null && juegoAModif.getRegionJP()) {%>
									<input type="checkbox" checked name="releaseCheckJP" value="Lanzado en JP"> <h6 style="display: inline">Lanzado en JP</h6> <br><br><br>
								<%} else { %>
									<input type="checkbox" name="releaseCheckJP" value="Lanzado en JP"> <h6 style="display: inline">Lanzado en JP</h6> <br><br><br>
								<%} %>
								<div>
									<h4 style="width: 50%; display: inline">Distribuidor</h4>
								</div>
								<select required class="form-control" name="disGame" style="width: 100%; display: inline">
									<option value="">None</option>
									<%
										if(listaDistribuidores.size() > 0){
											%>
												<c:forEach items="${listaDistribuidores}" var="abr">
													<option value="${abr.getDistribuidorId()}">${abr.getDistribuidorNombre()}</option>
												</c:forEach>
											<%
										}
									%>
								</select> 
						</div>
						<input type="submit" name="volverJuego" value="Volver" style="width: 29%; height: 60px; font-size: 24px; margin-left: 0px; margin-bottom: 40px; display: inline">
						<input type="submit" name="avanzarJuego" value="Aplicar y Avanzar" style="width: 70%; height: 60px; font-size: 24px; margin-left: 5px; margin-bottom: 40px; display: inline">
					</form>
				</div>
			</div>

		</div>
	</div>
	</section>
	<!-- ##### Contact Area End ##### -->

	<!-- ##### Footer Area Start ##### -->
	<footer class="footer-area"> <!-- Copywrite Area -->
	<div class="copywrite-content">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 col-sm-5">
					<!-- Copywrite Text -->
					<p class="copywrite-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>
							document.write(new Date().getFullYear());
						</script>
						All rights reserved |
					</p>
				</div>
				<div class="col-12 col-sm-7">
					<!-- Footer Nav -->
					<div class="footer-nav">
						<ul>
							<li><a href="index.html">IGDb</a></li>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="post.html">Búsqueda Avanzada</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>
</body>
