<%@page import="javax.websocket.Session"%>
<%@page import="javax.xml.ws.Response"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>IGDB - Listado Peticiones</title>

<!-- Favicon -->
<link rel="icon" href="images/core-img/favicon.ico">

<!-- Stylesheet -->
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area wow fadeInDown" data-wow-delay="500ms">
	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div
					class="col-12 d-flex align-items-center justify-content-between">
					<!-- Logo Area -->
					<div class="logo">
						<a href="Inicio.html"><img src="images/core-img/logo.png"
							alt=""></a>
					</div>

					<!-- Search & Login Area -->
					<div class="search-login-area d-flex align-items-center">
						<!-- Top Search Area -->
						<div class="top-search-area">
							<form action="#" method="post">
								<input type="search" name="top-search" id="topSearch"
									placeholder="Buscar">
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</form>
						</div>
						<!-- Login Area -->
						<%
                            	if(session.getAttribute("User")!=null)
                            	{
                            		if(!((Usuario)session.getAttribute("User")).getUsuarioAdmin())
                            			response.sendRedirect("Inicio.html");
                            		%>
						<div class="login-area">
							<a href="MisDatos.html"><span><%= ((Usuario)session.getAttribute("User")).getUsuarioNombre() %></span></a>
						</div>

						<div class="login-area">
							<a href="NuevaPeticion.html"><span>Pedir juego
									faltante</span></a>
						</div>

						<form method="post" action="Desconectar.html">
							<div class="login-area">
								<input type="submit" name="btnCerrarSesion" value="Desconectar"
									style="border-style: none" />
							</div>
						</form>

						<%
                            	}
                            	else
                            	{
                            		response.sendRedirect("Inicio.html");
                            	}
                            %>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="egames-main-menu" id="sticker">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="egamesNav">

				<!-- Navbar Toggler -->
				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<!-- Menu -->
				<div class="classy-menu">

					<!-- Close Button -->
					<div class="classycloseIcon">
						<div class="cross-wrap">
							<span class="top"></span><span class="bottom"></span>
						</div>
					</div>

					<!-- Nav Start -->
					<div class="classynav">
						<ul>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="Buscar.html">Búsqueda avanzada</a></li>
							<%
								if(session.getAttribute("User")!=null)
	                            {
	                            	if( ((Usuario)session.getAttribute("User")).getUsuarioNombre() != null && ((Usuario)session.getAttribute("User")).getUsuarioAdmin())
	                            	{
	                            	%>
	                            		<li><a href="ListarUsuariosAdmin.html">Listar usuarios</a></li>
										<li><a href="ListadoPeticiones.html">Listar peticiones</a></li>
										<li><a href="ABMLDesarrolladores.html">Desarrolladores</a></li>
										<li><a href="ABMLDistribuidores.html">Distribuidores</a></li>
										<li><a href="ABMLGeneros.html">Géneros</a></li>
										<li><a href="ABMLPaises.html">Países</a></li>
										<li><a href="ABMLPlataformas.html">Plataformas</a></li>
										<li><a href="AgregarJuego.html">Agregar Juego</a></li>
	                            	<%
	                            			}
	                            		}
                                    %>
						</ul>
					</div>
					<!-- Nav End -->
				</div>
				</nav>
			</div>
		</div>
	</div>
	</header>
	<%
		List<Peticion> listaPeticiones= null;
		//Obtiene los productos del controlador (Servlet)
		if(request.getAttribute("listaPeticiones")!=null){
			listaPeticiones =  (List<Peticion>) (request.getAttribute("listaPeticiones"));
		}
%>

	<table id="table1"
		class="container table table-bordered table-striped text-center"
		style="font-size: 17px; border-collapse: collapse !important">
		<thead>
			<tr>
				<th scope="col">ID</th>
				<th scope="col">Juego</th>
				<th scope="col">Detalle</th>
				<th scope="col">Usuario</th>
				<th scope="col">Cumplido</th>
			</tr>
		</thead>
		<tbody>
			<%	
		for(Peticion pet : listaPeticiones)
		{ %>
			<tr>
				<th scope="row"><%=pet.getPeticionId()%></th>
				<td><%=pet.getPeticionJuegoNombre()%></td>
				<td><%=pet.getPeticionDetalle()%></td>
				<td><%=pet.getPeticionUsuario().getUsuarioEmail()%></td>
				<% 
						if(pet.isPeticionCumplida())
						{
							%>
				<td>SI</td>
				
				<%
						}
						else
						{
							%>
				<td><form method="post" action="CumplirPeticion.html"> <input type="hidden" name="id" value="<%=pet.getPeticionId()%>"/>
					<input type="submit"  name="btnCumplir" value="Cumplir" style="width: 100px !important" /></form></td>
				<%
						}
					
					%>


			</tr>
			<%}%>
		</tbody>
	</table>

	<!-- ##### Footer Area Start ##### -->
	<footer class="footer-area"> <!-- Copywrite Area -->
	<div class="copywrite-content" style="padding-bottom: 80px">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 col-sm-5">
					<!-- Copywrite Text -->
					<p class="copywrite-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>document.write(new Date().getFullYear());</script>
						All rights reserved |
					</p>
				</div>
				<div class="col-12 col-sm-7">
					<!-- Footer Nav -->
					<div class="footer-nav">
						<ul>
							<li><a href="index.html">IGDb</a></li>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="post.html">Búsqueda Avanzada</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>

	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>
	<link rel="stylesheet" type="text/css"
		href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">


	<script type="text/javascript">
    
	    $(document).ready(function () {
	        $('#table1').DataTable();
	    });
    </script>

	<script type="text/javascript" charset="utf8"
		src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
</body>

</html>
