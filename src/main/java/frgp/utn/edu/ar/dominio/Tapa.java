package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table(name = "Tapas")
public class Tapa {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int TapaId;
		private String TapaNombre;
		
		public int getTapaId() {
			return TapaId;
		}
		public void setTapaId(int tapaId) {
			TapaId = tapaId;
		}
		public String getTapaNombre() {
			return TapaNombre;
		}
		public void setNombre(String tapaNombre) {
			this.TapaNombre = tapaNombre;
		}
		
		
		
}
