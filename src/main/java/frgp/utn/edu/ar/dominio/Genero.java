package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table (name = "Generos")
public class Genero {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int GeneroId;
	private String GeneroNombre;
	private String GeneroDescripcion;
	
	@Override
	public String toString() {
		return "Genero [GeneroId=" + GeneroId + ", GeneroNombre=" + GeneroNombre + ", GeneroDescripcion="
				+ GeneroDescripcion + "]";
	}

	public int getGeneroId() {
		return GeneroId;
	}

	public void setGeneroId(int generoId) {
		GeneroId = generoId;
	}

	public String getGeneroNombre() {
		return GeneroNombre;
	}

	public void setGeneroNombre(String generoNombre) {
		GeneroNombre = generoNombre;
	}

	public String getGeneroDescripcion() {
		return GeneroDescripcion;
	}

	public void setGeneroDescripcion(String generoDescripcion) {
		GeneroDescripcion = generoDescripcion;
	}
	
	
	
	
}
