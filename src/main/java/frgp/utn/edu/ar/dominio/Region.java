package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table (name = "Regiones")
public class Region {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer RegionId;
	private String RegionNombre;
	
	@Override
	public String toString() {
		return "Region [RegionId=" + RegionId + ", RegionNombre=" + RegionNombre + "]";
	}
	
	public int getRegionId() {
		return RegionId;
	}
	public void setRegionId(int regionId) {
		RegionId = regionId;
	}
	public String getRegionNombre() {
		return RegionNombre;
	}
	public void setRegionNombre(String regionNombre) {
		RegionNombre = regionNombre;
	}
	
}
