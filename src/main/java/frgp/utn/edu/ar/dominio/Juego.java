package frgp.utn.edu.ar.dominio;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table (name = "Juegos")
public class Juego {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int JuegoId;
	private String JuegoNombre;
	private String JuegoDescripcion;
	private String JuegoPromedioResenas;
	private String JuegoFechaLanzamiento;
	private Boolean regionUS;
	private Boolean regionEU;
	private Boolean regionJP;
	
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="DesarrolladorId")
	private Desarrollador JuegoDesarrollador;
	
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="DistribuidorId")
	private Distribuidor JuegoDistribuidor;
	
	@ManyToMany(cascade= {CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinTable(name="JuegosXPlataformas",joinColumns={@JoinColumn(name="JuegoId")}, inverseJoinColumns={@JoinColumn(name="PlataformaId")})
	private Set<Plataforma> lPlataformas= new HashSet<Plataforma>();
	
	@ManyToMany(cascade= {CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinTable(name="JuegoXGenero",joinColumns={@JoinColumn(name="JuegoId")}, inverseJoinColumns={@JoinColumn(name="GeneroId")})
	private Set<Genero> lGenero= new HashSet<Genero>();
	
	
	
	public Set<Genero> getlGenero() {
		return lGenero;
	}

	public void setlGenero(Set<Genero> lGenero) {
		this.lGenero = lGenero;
	}

	private boolean JuegoEstado = true;
	//private Tapa JuegoTapa;
	
	@Override
	public String toString() {
		return "Juego [JuegoId=" + JuegoId + ", JuegoNombre=" + JuegoNombre + ", JuegoDescripcion=" + JuegoDescripcion
				+ ", JuegoPromedioResenas=" + JuegoPromedioResenas + ", JuegoDesarrollador=" + JuegoDesarrollador + ", FechaLanzamiento=" + JuegoFechaLanzamiento + "]";
	}

	public int getJuegoId() {
		return JuegoId;
	}

	public void setJuegoId(int juegoId) {
		JuegoId = juegoId;
	}

	public String getJuegoNombre() {
		return JuegoNombre;
	}

	public void setJuegoNombre(String juegoNombre) {
		JuegoNombre = juegoNombre;
	}

	public String getJuegoDescripcion() {
		return JuegoDescripcion;
	}

	public void setJuegoDescripcion(String juegoDescripcion) {
		JuegoDescripcion = juegoDescripcion;
	}

	public String getJuegoPromedioResenas() {
		return JuegoPromedioResenas;
	}

	public void setJuegoPromedioResenas(String juegoPromedioResenas) {
		JuegoPromedioResenas = juegoPromedioResenas;
	}

	public String getJuegoFechaLanzamiento() {
		return JuegoFechaLanzamiento;
	}

	public void setJuegoFechaLanzamiento(String juegoFechaLanzamiento) {
		JuegoFechaLanzamiento = juegoFechaLanzamiento;
	}

	public Desarrollador getJuegoDesarrollador() {
		return JuegoDesarrollador;
	}

	public void setJuegoDesarrollador(Desarrollador juegoDesarrollador) {
		JuegoDesarrollador = juegoDesarrollador;
	}

	public Distribuidor getJuegoDistribuidor() {
		return JuegoDistribuidor;
	}

	public void setJuegoDistribuidor(Distribuidor juegoDistribuidor) {
		JuegoDistribuidor = juegoDistribuidor;
	}

	public Set<Plataforma> getlPlataformas() {
		return lPlataformas;
	}

	public void setlPlataformas(Set<Plataforma> lPlataformas) {
		this.lPlataformas = lPlataformas;
	}

	public boolean isJuegoEstado() {
		return JuegoEstado;
	}

	public void setJuegoEstado(boolean juegoEstado) {
		JuegoEstado = juegoEstado;
	}

	public Boolean getRegionUS()
	{
		return regionUS;
	}

	public void setRegionUS(Boolean regionUS)
	{
		this.regionUS = regionUS;
	}

	public Boolean getRegionEU()
	{
		return regionEU;
	}

	public void setRegionEU(Boolean regionEU)
	{
		this.regionEU = regionEU;
	}

	public Boolean getRegionJP()
	{
		return regionJP;
	}

	public void setRegionJP(Boolean regionJP)
	{
		this.regionJP = regionJP;
	}
	
	
	
}
