package frgp.utn.edu.ar.servicioImpl;

import java.text.DecimalFormat;
import java.util.ArrayList;

import frgp.utn.edu.ar.dao.PuntoDao;
import frgp.utn.edu.ar.dao.ResenaDao;
import frgp.utn.edu.ar.dominio.Punto;
import frgp.utn.edu.ar.dominio.Resena;
import frgp.utn.edu.ar.servicio.PuntoServicio;
import frgp.utn.edu.ar.servicio.ResenaServicio;

public class PuntoServicioImpl implements PuntoServicio{

private PuntoDao dataAccess = null;
	
	public void setDataAccess(PuntoDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	@Override
	public void insertarPunto(Punto punto) {
		dataAccess.insertarPunto(punto);
	}

	@Override
	public ArrayList<Punto> obtenerPuntos(){
		return dataAccess.obtenerPuntos();
	}
	
	@Override
	public void actualizarPunto(Punto punto) {
		dataAccess.actualizarPunto(punto);
	}

	@Override
	public int obtenerCantidadPuntos(Integer idResena) {
		return dataAccess.obtenerCantidadPuntos(idResena);
	}

	@Override
	public ArrayList<Punto> obtenerPunto(Integer idResena, Integer idUsuario){
		return dataAccess.obtenerPunto(idResena, idUsuario);
	}
	
	@Override
	public ArrayList<Punto> obtenerPuntosUsuario(Integer idUsuario){
		return dataAccess.obtenerPuntosUsuario(idUsuario);
	}

	@Override
	public void eliminarPunto(Punto punto) {
		dataAccess.eliminarPunto(punto);
	}
}
