package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.PaisDao;
import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.servicio.PaisServicio;

public class PaisServicioImpl implements PaisServicio{

private PaisDao dataAccess = null;
	
	public void setDataAccess(PaisDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Pais> obtenerPaises() {

		return dataAccess.obtenerPaises();
	}
	
	@Override
	public Pais obtenerPais(Integer id) {

		return dataAccess.obtenerPais(id);
	}

	@Override
	public void insertarPais(Pais pais) {

		dataAccess.insertarPais(pais);
		
	}

	@Override
	public void eliminarPais(Integer idPais) {

		dataAccess.eliminarPais(idPais);
		
	}

	@Override
	public void actualizarPais(Pais pais) {

		dataAccess.actualizarPais(pais);
		
	}
	
}
