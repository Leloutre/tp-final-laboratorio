package frgp.utn.edu.ar.controllers;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.dominio.Distribuidor;
import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.servicio.DesarrolladorServicio;
import frgp.utn.edu.ar.servicio.DesarrolladorServicio;
import frgp.utn.edu.ar.servicio.PaisServicio;

@Controller
public class DesarrolladorController {
	
	@Autowired
	public  DesarrolladorServicio service;
	@Autowired
	public PaisServicio servicePais;
	
//	public void init(ServletConfig config) {
//		ApplicationContext ctx = WebApplicationContextUtils
//				.getRequiredWebApplicationContext(config.getServletContext());
//		
//		this.service = (DesarrolladorServicio) ctx.getBean("serviceDesarrolladorBean");
//		this.servicePais = (PaisServicio) ctx.getBean("ServicePaisBean");
//	}
	@RequestMapping("ABMLDesarrolladores.html")
	public ModelAndView ABMLDesarrolladores() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDesarrolladores");
	    return MV;
	}
	
	@RequestMapping(value="ModificarDesarrollador.html", params = "eliminarDev")
	public ModelAndView EliminarDesarrollador(Integer desarrolladores) {
	    ModelAndView MV = new ModelAndView();
	    Desarrollador desarrollador = service.obtenerDesarrollador(desarrolladores);
	    String mensaje = "Eliminado exitoso.";
	    if(desarrollador != null){
	    	try {
	    		service.eliminarDesarrollador(desarrolladores);
		    }
		    catch(Exception e) {
		    	mensaje = "El Desarrollador no puede ser eliminado ya que est� en uso.";
		    }
	    }
	    MV.addObject("mensajeResultado", mensaje);
	    MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDesarrolladores");
	    return MV;
	}
	
	@RequestMapping(value="ModificarDesarrollador.html", params = "modificarDev")
	public ModelAndView ModificarDesarrollador(Integer desarrolladores) {
	    ModelAndView MV = new ModelAndView();
	    Desarrollador desarrollador = service.obtenerDesarrollador(desarrolladores);
	    if(desarrollador != null)
	    	MV.addObject("desarrolladorAModif", desarrollador);
	    MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDesarrolladores");
	    return MV;
	}
	
	@RequestMapping("AgregarDesarrollador.html")
	public ModelAndView agregarDesarrollador(String idDev, String nameDev, String createdDev, String paisDev) {
	    ModelAndView MV = new ModelAndView();
	    
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Desarrollador desarrollador = (Desarrollador)context.getBean("DesarrolladorBean");
		
		if(idDev != "") {
			desarrollador = this.service.obtenerDesarrollador(Integer.parseInt(idDev));	
			desarrollador.setDesarrolladorNombre(nameDev);
			desarrollador.setDesarrolladorFechaCreacion(createdDev);
			desarrollador.setDesarrolladorPais(servicePais.obtenerPais(Integer.parseInt(paisDev)));
			this.service.actualizarDesarrollador(desarrollador);
		}
		else {
			desarrollador.setDesarrolladorNombre(nameDev);
			desarrollador.setDesarrolladorFechaCreacion(createdDev);
			desarrollador.setDesarrolladorPais(servicePais.obtenerPais(Integer.parseInt(paisDev)));
			this.service.insertarDesarrollador(desarrollador);
		}

	    MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDesarrolladores");
	    return MV;
	}
	
	
	
	@RequestMapping("/abrirDesarrolladores.html")
	public ModelAndView redireccion(){
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
		MV.addObject("listaPaises", this.servicePais.obtenerPaises());
		MV.setViewName("Desarrolladores"); 
		return MV;
	}
	
	
	@RequestMapping(value ="/altaDesarrollador.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView validarDesarrollador(String nombreD, String creacionD, Pais paisD){
		ModelAndView MV = new ModelAndView();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Desarrollador x = (Desarrollador)context.getBean("DesarrolladorBean");	
		//tal vez si persistimos el objeto que hicimos con el bean y despues hacemos context.close() no se borra?
		x.setDesarrolladorNombre(nombreD);
		x.setDesarrolladorFechaCreacion(creacionD);
		x.setDesarrolladorPais(paisD);
		
		String Message="";
		
		try{
			
			service.insertarDesarrollador(x);
			Message = "Desarrollador agregado";
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el Desarrollador";
		}
		finally
		{
		
		}
	
		MV.setViewName("Desarrolladores");
		MV.addObject("Mensaje", Message);
		MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
		//lista paises?
		MV.setViewName("Desarrolladores"); 
		return MV;
		
	}
	
     
	@RequestMapping(value ="/eliminarDesarrollador.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView eliminarDesarrollador(Integer id){
		ModelAndView MV = new ModelAndView();
		service.eliminarDesarrollador(id);
		MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
		MV.setViewName("Desarrolladores"); 
		MV.addObject("Mensaje", "Desarrollador eliminado");
		return MV;
	}
	/*
	@RequestMapping(value = { "/delete-user-{ssoId}" }, method = RequestMethod.GET)
    public ModelAndView borrarDesarrollador(@PathVariable Integer ssoId) {
		service.eliminarDesarrollador(ssoId);
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Desarrolladores");
		
		//Actualiza los generos
		MV.addObject("listaDesarrolladores",this.service.obtenerDesarrolladores());
		MV.setViewName("Desarrolladores"); 
		return MV;
    }*/
}
