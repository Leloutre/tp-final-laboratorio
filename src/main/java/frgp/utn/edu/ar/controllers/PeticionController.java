package frgp.utn.edu.ar.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.*;
import frgp.utn.edu.ar.servicio.PeticionServicio;

@Controller
public class PeticionController {

	@Autowired
	public  PeticionServicio service;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		this.service = (PeticionServicio) ctx.getBean("servicePeticionBean");
	}
	
	@RequestMapping("Peticion.html")
	public ModelAndView Peticion(HttpServletRequest request, String peticionNombre, String peticionDetalles) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Usuario user = (Usuario)request.getSession().getAttribute("User");
		String respuesta = "";
		ModelAndView MV = new ModelAndView();
		
		Peticion pet = (Peticion)context.getBean("PeticionBean");
		pet.setPeticionCumplida(false);
		pet.setPeticionDetalle(peticionDetalles);
		pet.setPeticionJuegoNombre(peticionNombre);
		pet.setPeticionUsuario(user);
		
		this.service.insertarPeticion(pet);
		
		respuesta = "�Petici�n generada con �xito!";
	
		MV.addObject("respuesta", respuesta);		
		MV.setViewName("Usuario/NuevaPeticion");
		return MV;
		
	}
	
	@RequestMapping("ListadoPeticiones.html")
	public ModelAndView ListadoPeticiones() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaPeticiones",this.service.obtenerPeticiones());
	    MV.setViewName("Admin/ListadoPeticiones");
	    return MV;
	}
	
	@RequestMapping("CumplirPeticion.html")
	public ModelAndView CumplirPeticion(Integer id) {
	    ModelAndView MV = new ModelAndView();
	    Peticion pet = this.service.obtenerPeticion(id);
	    pet.setPeticionCumplida(true);
	    this.service.actualizarPeticion(pet);
	    MV.addObject("listaPeticiones",this.service.obtenerPeticiones());
	    MV.setViewName("Admin/ListadoPeticiones");
	    return MV;
	}
	
	//TODO metodos Peticion
	
}
