package frgp.utn.edu.ar.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Juego;
import frgp.utn.edu.ar.servicio.JuegoServicio;

@Controller
public class RedirectController {
	@Autowired
	public  JuegoServicio serviceJuego;
	
	@RequestMapping("Inicio.html")
	public ModelAndView Inicio() {
	
	List<Juego> juegosTop = new ArrayList<>(serviceJuego.queryJuego("FROM Juego order by JuegoPromedioResenas desc"));
	
	List<Juego> juegosNew = new ArrayList<>(serviceJuego.queryJuego("FROM Juego order by JuegoFechaLanzamiento desc"));
		
	ModelAndView MV = new ModelAndView();
	MV.addObject("JuegosTop", juegosTop);
	MV.addObject("JuegosNew", juegosNew);
	MV.setViewName("Usuario/Inicio");
	return MV;
	}

	@RequestMapping("Login.html")
	public ModelAndView IrLogin() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/Login");
	    return MV;
	}

	@RequestMapping("Registro.html")
	public ModelAndView IrRegistro() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/Registro");
	    return MV;
	}

	@RequestMapping("Juego.html")
	public ModelAndView IrJuego() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/Juego");
	    return MV;
	}

	@RequestMapping("CambiarContrasena.html")
	public ModelAndView CambiarContrasena() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/CambiarContrasena");
	    return MV;
	}

	@RequestMapping("ListadoAdmin.html")
	public ModelAndView ListadoAdmin() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Admin/ListadoAdmin");
	    return MV;
	}

	@RequestMapping("MisDatos.html")
	public ModelAndView MisDatos() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/MisDatos");
	    return MV;
	}

	@RequestMapping("NuevaPeticion.html")
	public ModelAndView NuevaPeticion() {
	    ModelAndView MV = new ModelAndView();
	    MV.setViewName("Usuario/NuevaPeticion");
	    return MV;
	}
}
