package frgp.utn.edu.ar.controllers;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Genero;
import frgp.utn.edu.ar.servicio.GeneroServicio;

@Controller
public class GeneroController {

	@Autowired
	public  GeneroServicio service;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		this.service = (GeneroServicio) ctx.getBean("serviceGeneroBean");
	}
	
		@RequestMapping("ABMLGeneros.html")
		public ModelAndView ABMLGeneros() {
		    ModelAndView MV = new ModelAndView();
		    MV.addObject("listaGeneros",this.service.obtenerGeneros());
		    MV.setViewName("ABML/ABMLGeneros");
		    return MV;
		}
		
		@RequestMapping(value="ModificarGenero.html", params = "eliminarGenero")
		public ModelAndView EliminarGenero(Integer generos) {
		    ModelAndView MV = new ModelAndView();
		    Genero genero = service.obtenerGenero(generos);
		    if(genero != null)
		    	service.eliminarGenero(generos);
		    MV.addObject("listaGeneros",this.service.obtenerGeneros());
		    MV.setViewName("ABML/ABMLGeneros");
		    return MV;
		}
		
		@RequestMapping(value="ModificarGenero.html", params = "modificarGenero")
		public ModelAndView ModificarGenero(Integer generos) {
		    ModelAndView MV = new ModelAndView();
		    Genero genero = service.obtenerGenero(generos);
		    if(genero != null)
		    	MV.addObject("generoAModif", genero);
		    MV.addObject("listaGeneros",this.service.obtenerGeneros());
		    MV.setViewName("ABML/ABMLGeneros");
		    return MV;
		}
		
		@RequestMapping("AgregarGenero.html")
		public ModelAndView agregarGenero(String idGen, String nameGen, String descGen) {
		    ModelAndView MV = new ModelAndView();
		    
		    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			
			Genero genero = (Genero)context.getBean("GeneroBean");
			
			if(idGen != "") {
				genero = this.service.obtenerGenero(Integer.parseInt(idGen));	
				genero.setGeneroNombre(nameGen);
				genero.setGeneroDescripcion(descGen);
				this.service.actualizarGenero(genero);
			}
			else {
				genero.setGeneroNombre(nameGen);
				genero.setGeneroDescripcion(descGen);
				this.service.insertarGenero(genero);
			}

		    MV.addObject("listaGeneros",this.service.obtenerGeneros());
		    MV.setViewName("ABML/ABMLGeneros");
		    return MV;
		}
	
		@RequestMapping("/abrirGeneros.html")
		public ModelAndView redireccion(){
			ModelAndView MV = new ModelAndView();
			MV.addObject("listaGeneros",this.service.obtenerGeneros());
			MV.setViewName("Generos"); 
			return MV;
		}
		
		
		@RequestMapping(value ="/altaGenero.html" , method= { RequestMethod.GET, RequestMethod.POST})
		public ModelAndView validarGenero(String nombreG, String descripcionG){
			ModelAndView MV = new ModelAndView();
			
			ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			Genero x = (Genero)context.getBean("GeneroBean");	
			//tal vez si persistimos el objeto que hicimos con el bean y despues hacemos context.close() no se borra?
			x.setGeneroNombre(nombreG);
			x.setGeneroDescripcion(descripcionG);
			
			String Message="";
			
			try{
				
				service.insertarGenero(x);
				Message = "G�nero agregado";
			}
			catch(Exception e)
			{
				Message = "No se pudo insertar el g�nero";
			}
			finally
			{
			
			}
		
			MV.setViewName("Generos");
			MV.addObject("Mensaje", Message);
			MV.addObject("listaGeneros",this.service.obtenerGeneros());
			MV.setViewName("Generos"); 
			return MV;
			
		}
}
