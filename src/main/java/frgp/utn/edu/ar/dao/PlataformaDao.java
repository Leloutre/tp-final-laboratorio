package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Plataforma;

public interface PlataformaDao {

	public void insertarPlataforma(Plataforma plataforma);

	public ArrayList<Plataforma> obtenerPlataformas();

	public void eliminarPlataforma(Integer idPlataforma);

	public void actualizarPlataforma(Plataforma plataforma);
	
	public Plataforma obtenerPlataforma(Integer idPlataforma);
	
}
