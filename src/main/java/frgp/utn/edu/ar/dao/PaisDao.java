package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Pais;

public interface PaisDao {
	
	public void insertarPais(Pais pais);

	public ArrayList<Pais> obtenerPaises();

	public void eliminarPais(Integer idPais);

	public void actualizarPais(Pais pais);

	Pais obtenerPais(Integer id);
}
