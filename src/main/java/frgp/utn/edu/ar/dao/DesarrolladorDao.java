package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Desarrollador;

public interface DesarrolladorDao {

			//Alta de persona
			public void insertarDesarrollador(Desarrollador genero);

			//Obtiene todas las presonas
			public ArrayList<Desarrollador> obtenerDesarrolladores();

			//Elimina una presona a aprtir del dni
			public void eliminarDesarrollador(Integer idDesarrollador);

			//Actualiza los datos de una persona
			public void actualizarDesarrollador(Desarrollador genero);
			
			//Obtiene un dev por id
			public Desarrollador obtenerDesarrollador(Integer id);
	
}
