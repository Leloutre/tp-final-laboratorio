package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Peticion;

public interface PeticionDao {

	public void insertarPeticion(Peticion Peticion);

	public ArrayList<Peticion> obtenerPeticiones();

	public void eliminarPeticion(Integer idPeticion);

	public void actualizarPeticion(Peticion Peticion);

	Peticion obtenerPeticion(Integer id);
	
}
