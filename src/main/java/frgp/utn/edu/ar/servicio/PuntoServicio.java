package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Punto;
import frgp.utn.edu.ar.dominio.Resena;

public interface PuntoServicio {
	public void insertarPunto(Punto punto);

	public ArrayList<Punto> obtenerPuntos();

	public void actualizarPunto(Punto punto);

	public int obtenerCantidadPuntos(Integer idResena);

	public ArrayList<Punto> obtenerPunto(Integer idResena, Integer idUsuario);

	public void eliminarPunto(Punto punto);

	ArrayList<Punto> obtenerPuntosUsuario(Integer idUsuario);
	
}
