package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Peticion;

public interface PeticionServicio {

	ArrayList<Peticion> obtenerPeticiones();

	void insertarPeticion(Peticion Peticion);

    void eliminarPeticion(Integer idPeticion);

	void actualizarPeticion(Peticion Peticion);

	Peticion obtenerPeticion(Integer id);
	
}
