package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.JuegoDao;
import frgp.utn.edu.ar.dominio.Juego;

public class JuegoDaoImpl implements JuegoDao {
	
	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public Integer insertarJuego(Juego juego) {
		Integer ret = (Integer) this.hibernateTemplate.save(juego);
		return ret;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Juego> obtenerJuegos() {
		return (ArrayList<Juego>) this.hibernateTemplate.loadAll(Juego.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarJuego(Integer idJuego) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Juego juego = (Juego)context.getBean("JuegoBean");

		juego.setJuegoId(idJuego);
		this.hibernateTemplate.delete(juego);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarJuego(Juego juego) {
		this.hibernateTemplate.update(juego);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Juego obtenerJuego(Integer idJuego) {
		return this.hibernateTemplate.get(Juego.class, idJuego);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Juego> buscarJuegosPorNombre(String nombreJuego) {
		return (ArrayList<Juego>) this.hibernateTemplate.find("FROM Juego AS J WHERE J.JuegoNombre LIKE '%" + nombreJuego + "%'");
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Juego> queryJuego(String query) {
		return (ArrayList<Juego>) this.hibernateTemplate.find(query);
	}
}
