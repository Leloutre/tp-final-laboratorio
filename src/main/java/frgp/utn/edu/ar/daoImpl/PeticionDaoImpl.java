package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.PeticionDao;
import frgp.utn.edu.ar.dao.PuntoDao;
import frgp.utn.edu.ar.dominio.Peticion;
import frgp.utn.edu.ar.dominio.Punto;
import frgp.utn.edu.ar.dominio.Resena;

public class PeticionDaoImpl implements PeticionDao{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarPeticion(Peticion peticion) {
		this.hibernateTemplate.save(peticion);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Peticion> obtenerPeticiones() {
		return (ArrayList<Peticion>) this.hibernateTemplate.loadAll(Peticion.class);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Peticion obtenerPeticion(Integer id) {
		return this.hibernateTemplate.get(Peticion.class, id);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarPeticion(Integer idPeticion) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Peticion peticion = (Peticion)context.getBean("PeticionBean");

		peticion.setPeticionId(idPeticion);
		this.hibernateTemplate.delete(peticion);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarPeticion(Peticion peticion) {
		this.hibernateTemplate.update(peticion);
	}
	
}
